#!/bin/bash

clear #clear all the text in the terminal so that the next message is easier to read

echo "Hi, $USER"

echo "Starting this script..."

sleep 1s

echo "Please type your password"

#To open all software repositories in your ubuntu based version, so that you can install Skype, flash, etc
sudo sed 's/# deb/deb/' -i /etc/apt/sources.list

#Choose the laguage of the applications
echo "Hello again $USER
Choose the System language, write the number of the option:
Portuguese (1)
French     (2)
English    (3)
German     (4)"
read "opcao" #Users reply

echo "This should take around 30 minutes. So go for some coffe, a smoke or a beer..."
sleep 5s

#To use the most updated software sources when installing things
sudo apt update
sudo apt-get --yes dist-upgrade
#To accept EULA for MS fonts (Arial, Times new roman, etc.)
sudo echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true |sudo debconf-set-selections

#To detect which ubuntu based distro you are using, and install the correct restriced-extras package according to your ubuntu based distro, with all the audio and video codecs, Microsoft fonts in libreoffice such as Arial, Times New Roman, flash, etc.

desktop=$(echo $DESKTOP_SESSION | tr '[:upper:]' '[:lower:]')
echo " Desktop in use: $desktop"
sleep 2s

#Xubuntu
if [ "$desktop" = "xubuntu" ]; then
	sudo apt install --yes xubuntu-restricted-extras
fi
#Lubuntu
if [ "$desktop" = "lubuntu" ]; then
	sudo apt install --yes lubuntu-restricted-extras
fi
#Mint
if [ "$desktop" = "mint" ]; then
	sudo apt install --yes ubuntu-restricted-extras
fi
#Ubuntu (Default)
if [ "$desktop" = "ubuntu" ]
	sudo apt install --yes ubuntu-restricted-extras
fi

if [ "$desktop" = "lubuntu" ]
then
    sudo apt install --yes gcc make
fi

#Utilities
sudo apt install -y $(cat utilitarios)
#Internet
sudo apt install -y $(cat Internet)
#Install addons in firefox
bash ./firefox-config.sh
#PPA and Snaps
bash ./PPAandSNAP
#Encryption tools
sudo apt install -y $(cat encryption)
#Office applications
sudo apt install -y $(cat office)
#Graphics
sudo apt install -y $(cat office)
#Video
sudo apt install -y $(cat video)
#Sound
sudo apt install -y $(cat sound)
#DVD
sudo apt install -y $(cat dvd) && sudo /usr/share/doc/libdvdread4/install-css.sh
#To remove. This part removes aplications that are redundant.
sudo apt-get --yes purge $(cat purge)

clear #clear all the text in the terminal so that the next message is easier to read

#Some system configs

echo "# De-activate error-system"
sudo sed -i 's/enabled=1/enabled=0/g' /etc/default/apport

#echo "# Notification for Release Upgrades: never"
#sudo sed -i 's/Prompt=.*/Prompt=never/g' /etc/update-manager/release-upgrades

if [ "$opcao" = "1" ]; then
 echo "installing (PT) language support..." && sleep 2s &&
	sudo apt install $(check-language-support -l pt) -y
fi

if [ "$opcao" = "1" ]; then
	echo "installing (FR) language support..." && sleep 2s &&
		sudo apt install $(check-language-support -l fr) -y
fi

if [ "$opcao" = "3" ]; then
	echo "installing (EN) language support..." && sleep 2s &&
		sudo apt install $(check-language-support -l uk) -y
fi

if [ "$opcao" = "4" ]; then
	echo "installing (DE) language support..." && sleep 2s &&
		sudo apt install $(check-language-support -l de) -y
fi

#Do a system cleaning
sudo apt --yes -f install
sudo apt --yes autoremove
sudo apt --yes clean

echo "Hello again $USER"

echo "It's finished!!! All instalations and updates are complete."
echo "We recomend you to reboot your computer to finish the updates."

read -r -p "Reboot your computer now? [Y/n]" response
 response=${response,,}
 if [[ $response =~ ^(yes|y| ) ]]; then
clear ; echo "Rebooting..." ; sleep 2s ; sudo reboot
else
clear ; echo "Exiting the terminal..." ; sleep 2s ; exit ; exit
fi
