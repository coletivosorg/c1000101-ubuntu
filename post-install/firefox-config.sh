

echo "Fire firefox needs be opened and closed to create a user folder for this script to work"

echo "This script will do that automatically"
#sleep 10s
firefox & sleep 20s ; killall firefox
ID2=$(ls ~/.mozilla/firefox | grep .default)
mkdir ~/.mozilla/firefox/$ID2/extensions

mkdir /tmp/ffadons
cd /tmp/ffadons
#https Everywhere
wget https://addons.mozilla.org/firefox/downloads/latest/https-everywhere/addon-229918-latest.xpi
unzip -p *.xpi install.rdf > id1.txt
ID1=$(cat id1.txt | grep "<em:id>" -m 1 | sed "s/.*>\(.*\)<.*/\1/")
cp *.xpi $ID1.xpi
cp *.xpi ~/.mozilla/firefox/$ID2/extensions
rm *.xpi && rm *.txt
#NO Script
wget https://addons.mozilla.org/firefox/downloads/latest/noscript/addon-722-latest.xpi
unzip -p *.xpi install.rdf > id1.txt
ID1=$(cat id1.txt | grep "<em:id>" -m 1 | sed "s/.*>\(.*\)<.*/\1/")
cp *.xpi $ID1.xpi
cp *.xpi ~/.mozilla/firefox/$ID2/extensions
rm *.xpi && rm *.txt
#Self destructing cookies
wget https://addons.mozilla.org/firefox/downloads/latest/self-destructing-cookies/addon-415846-latest.xpi
unzip -p *.xpi install.rdf > id1.txt
ID1=$(cat id1.txt | grep "<em:id>" -m 1 | sed "s/.*>\(.*\)<.*/\1/")
cp *.xpi $ID1.xpi
cp *.xpi ~/.mozilla/firefox/$ID2/extensions
rm *.xpi && rm *.txt
#youtube-all-html5
wget https://addons.mozilla.org/firefox/downloads/latest/youtube-all-html5/addon-438608-latest.xpi
unzip -p *.xpi install.rdf > id1.txt
ID1=$(cat id1.txt | grep "<em:id>" -m 1 | sed "s/.*>\(.*\)<.*/\1/")
cp *.xpi $ID1.xpi
cp *.xpi ~/.mozilla/firefox/$ID2/extensions
rm *.xpi && rm *.txt
#Random Agent Spoofer
wget https://addons.mozilla.org/firefox/downloads/latest/random-agent-spoofer/addon-473878-latest.xpi
unzip -p *.xpi install.rdf > id1.txt
ID1=$(cat id1.txt | grep "<em:id>" -m 1 | sed "s/.*>\(.*\)<.*/\1/")
cp *.xpi $ID1.xpi
cp *.xpi ~/.mozilla/firefox/$ID2/extensions
rm *.xpi && rm *.txt

cd ~
rm -rf /tmp/ffadons
