# After install for ubuntu based distros
This is a script for post installations. It automatically installs some of the most common programs in Linux installations; libreoffice, vlc, codecs, flash, thunderbird, clementine, audio and video editing tools, etc.

We recommend that before running this script you read it so that you know what this does to your computer, and see what it installs. But that's your choice ;)

It is compatible with both 32 and 64 bit architecture, and is compatible with any Ubuntu based distro (ex:Lubuntu, Xubuntu, Linux Mint, Ubuntu Mate, Elementary OS, and many others), except KUBUNTU, sorry...

You can see the full list of Ubuntu based distros here:

```
https://upload.wikimedia.org/wikipedia/commons/7/79/UbuntuFamilyTree1210.svg
```
